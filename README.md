## Getting Started
This project is a basic task manager application that lets a user create, view, update, and delete tasks they're keeping track of. 
Before you get started please download the [mock-task-service](https://gitlab.com/william.andrade/mock-task-service) and make sure you have it running in the background. 
This project won't work properly without it!

## Set Up Instructions and Commands

In your terminal, navigate to the task-manager directory, and from the root folder run:

- `npm install` - installs all project dependencies. 

- `npm start` - Runs the app in the development mode. Open [http://localhost:4000](http://localhost:4000) to view it in the browser.
the page will reload if you make edits and show errors in the console if there are any.

### Additional Commands
- `npm test` - launches the test runner in interactive watch mode. To run all the tests, press `a` on your keyboard.
- `npm run build` - builds the app for production to the `build` folder. 


## Technologies Used 
For this UI project, [Create React App](https://github.com/facebook/create-react-app) was used to get started on development right away.
Another useful feature was that a testing framework ([Jest](https://jestjs.io/)) was included as well. 
I also chose to use [Enzyme](https://enzymejs.github.io/enzyme/) in addition to Jest to test my react components output more thoroughly.  

For styling, I decided to use SCSS. Mainly because of variable usage and nested styling class declarations. 
One simple advantage to that is being able to establish your styles in the same hierarchy as the components are in your component files. 
And lastly, I used [React Router](https://reactrouter.com/) to handle routes for component view changes. 

## Component Design
Below you'll find some screenshots that highlight some main components used to build the application.
The Login Screen is made up of two components, a footer and the main form. 

#### Login Screen
![Component Design Image](login_screen.png)

### Task Manager Screen
The task manager screen is made up of four components: A container component, a compositional layout component, header, and a task card component. 
The TaskLayout component is used to provide the Header component throughout the entire application. 
![Component Design Image](task_manager.png)

### Task Detail Screen
This component is where the bulk of the app's functionality lives. From this screen, you can update and delete tasks. This component is made up only of native HTML DOM elements.
 
![Component Design Image](task_details.png)



## Additional Notes and Assumptions Made:
- To log in, use these two values as credentials:
    - email: test@test.com
    - password: password
- For the title and task description inputs, I chose an arbitrary value to limit the number of characters for each field.
- When creating a new task, I chose to disable the [Save] button until all fields had values.
- You can sort by the `dueDate` field in ascending and descending order.
- No component libraries are used this application.
- I know with React 16 hooks were introduced. I see the value, but I chose not to use them in this application. The main reason being that there's a good amount of logic involved for most of the components.
  Class components offer good structure and make it easier to follow the logic involved with certain actions. With functional components and hooks, I believe things would have gotten bloated and disorganized fast. 
- In regard to improvements for the overall project a few things that I would include would be:
    - e2e tests to properly tests the services I implemented
    - more unit tests for better code coverage
    - develop a more generic input field component 
    - support for a better object structure
    - protective routing!
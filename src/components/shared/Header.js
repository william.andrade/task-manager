import React from "react";
import "../../styles/Header.scss";

function Header(props) {
  const handleLogout = () => {
    // redirect back to login screen
    props.history.push("/");
  };

  const handleTaskManagerRedirect = () => {
      props.history.push("/task-manager");
  }

  return (
    <div className="header-container">
      <header className="tasks-header" onClick={handleTaskManagerRedirect}>Task Manager</header>
      <button type="submit" className="logout-button" onClick={handleLogout}>
        Log out
      </button>
    </div>
  );
}

export default Header;

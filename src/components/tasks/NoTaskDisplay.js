import React from "react";
import { NO_TASKS_MESSAGE } from "../../constants";
import ExclamationPointIcon from "../../assets/exclamation-point.svg";
import "../../styles/NoTaskDisplay.scss";

function NoTaskDisplay() {
  return (
    <div className="display-container">
      <p>{NO_TASKS_MESSAGE}</p>
      <img src={ExclamationPointIcon} alt="" />
    </div>
  );
}

export default NoTaskDisplay;

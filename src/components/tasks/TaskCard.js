import React, { Component } from "react";
import { formatDate } from "../../utils/utils";
import {COMPLETED_STATUS, NOT_COMPLETED} from "../../constants";
import "../../styles/TaskCard.scss";

class TaskCard extends Component {
  handleTaskClick = (task) => {
    // handler for the entire task

    // When routing to the TaskDetail component, pass the taskId so we can fetch it from the server as soon as component mounts
    this.props.history.push({
      pathname: `/task-manager/task/${task.id}`,
      state: { taskId: task.id },
    });
  };

  handleStatusChange = (event) => {
    // handler for the check box
    event.stopPropagation();

    const newTask = { ...this.props.task };

    if (newTask.status === COMPLETED_STATUS) {
      newTask.status = NOT_COMPLETED;
    } else if (newTask.status === NOT_COMPLETED) {
      newTask.status = COMPLETED_STATUS;
    }
    // component can"t change it"s own props
    // need to pass this to parent for updating
    this.props.onTaskStatusChange(newTask);
  };

  render() {
    const { task } = this.props;
    return (
      <div className="task-card-container">
        <button
          className="task-card"
          onClick={() => this.handleTaskClick(task)}
        >
          <div>
            <input
              className="tasks-status"
              type="checkbox"
              name="status"
              value={task.status}
              defaultChecked={task.status === COMPLETED_STATUS}
              onClick={this.handleStatusChange}
            />
            <span className="task-title">{task.title}</span>
            <span className="task-due-date">{formatDate(task.dueDate)}</span>
          </div>

          <div className="description-container">
            <div>{task.description}</div>
          </div>
        </button>
      </div>
    );
  }
}

export default TaskCard;

import React, { Component } from "react";
import { getTask } from "../../services/taskService";
import TaskLayout from "../containers/TaskLayout";
import { updateTask, deleteTask, postNewTask } from "../../services/taskService";
import { setMinDate } from "../../utils/utils";
import "../../styles/TaskDetails.scss";

class TaskDetails extends Component {
  state = {
    taskData: {
      title: "",
      description: "",
      dueDate: "",
      status: "",
    },
  };

  componentDidMount() {
    const {location, match} = this.props;
    const taskId = match.params.id ? match.params.id : '';
    const isNew = location.state ? location.state.new : false;
    const newTask = location.state ? location.state.taskData : {};

    if (taskId && !isNew) {
      getTask(taskId).then((data) => {
        this.setState({taskData: data});
      })
    } else {
      this.setState({taskData: newTask})
    }
  }

  handleInputChange = (event) => {
    const value = event.target.value;
    const name = event.target.name;

    const updatedTask = { ...this.state.taskData };
    updatedTask[name] = value;

    this.setState({ taskData: updatedTask });
  };

  handleTaskSave = (event, taskData) => {
    event.preventDefault();
    const {location, history} = this.props;
    const isNew = location.state.new ? location.state : false;

    if(isNew) { // if it's a new task, post it to the server
      postNewTask(taskData).then((data) => {
        history.push({
          pathname: "/task-manager/",
        })
      })
    } else { // otherwise it's an update
      const updatedTask = { ...taskData };
      updateTask(updatedTask).then((updatedData) => {
        history.push({
          pathname: "/task-manager", // route back to TaskManager view
          state: { tasks: updatedData },
        });
      });
    }
  };

  handleTaskDelete = (event, taskData) => {
    event.preventDefault();
    deleteTask(taskData).then((updatedData) => {
      this.props.history.push({
        // route back to TaskManager view
        pathname: "/task-manager",
        state: { tasks: updatedData },
      });
    });
  };

  render() {
    const { title, description, dueDate, status } = this.state.taskData;
    const disabled = !title || !description || !dueDate || !status;

    return (
      <TaskLayout history={this.props.history}>
        <form className="details-form">
          <label htmlFor="title" className="labels">
            Title
          </label>
          <input
            type="text"
            className="title-input"
            name="title"
            maxLength={100}
            size={40}
            required={true}
            value={title}
            onChange={this.handleInputChange}
          />

          <label htmlFor="description" className="labels">
            Description
          </label>
          <textarea
            type="text"
            name="description"
            className="form-description"
            maxLength={500}
            required={true}
            value={description}
            onChange={this.handleInputChange}
          />

          <label htmlFor="due-date" className="labels">
            Due Date
          </label>
          <input
            id="dueDate"
            type="date"
            name="dueDate"
            min={setMinDate()}
            required={true}
            value={dueDate}
            onChange={this.handleInputChange}
            className="form-date"
          />

          <div className="status-container">
            <label htmlFor="status" className="labels">
              Status
            </label>
            <select name="status" value={status} onChange={this.handleInputChange}>
              <option value="Completed">Completed</option>
              <option value="Not completed">Not Completed</option>
            </select>
          </div>
        </form>
        <div className="buttons-container">
          <button
            type="submit"
            className="save-button"
            style={{ backgroundColor: disabled ? "grey" : "#ffcc00" }}
            disabled={disabled}
            onClick={(event) => this.handleTaskSave(event, this.state.taskData)}
          >
            Save
          </button>
          <button
            type="submit"
            className="delete-button"
            onClick={(event) =>
              this.handleTaskDelete(event, this.state.taskData)
            }
          >
            Delete
          </button>
        </div>
      </TaskLayout>
    );
  }
}

export default TaskDetails;

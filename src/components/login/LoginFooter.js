import React from "react";
import "../../styles/LoginFooter.scss";

function LoginFooter(props) {
  const { onLoginClick, disabled } = props;
  return (
    <div className="footer">
      <button
        type="submit"
        id="login-button"
        onClick={onLoginClick}
        disabled={disabled}
        style={{ backgroundColor: disabled ? "grey" : "#25bb87" }}
      >
        Login
      </button>
    </div>
  );
}

export default LoginFooter;

import React, { Component } from "react";
import { login } from "../../services/authService";
import Clipboard from "../../assets/clipboard.svg";
import LoginFooter from "./LoginFooter";
import "../../styles/LoginForm.scss";

class LoginForm extends Component {
  state = {
    email: "",
    password: "",
    errorMessage: "",
  };

  handleInputChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({ [name]: value, errorMessage: "" });
  };

  taskManagerLogin() {
    const {email, password} = this.state;
    login(email, password).then((response) => {
      if (response.data === null) {
        this.setState({errorMessage: response.message});
      } else {  // If login successful, route to TaskCard view
        this.props.history.push({pathname: "/task-manager"});
      }
    });
  }

  handleLogin = (event) => {
    event.preventDefault();
    this.taskManagerLogin();
  };

  handleKeyDown = (event) => {
    const {email, password} = this.state;
    const values = email.length && password.length;
    if(event.key === 'Enter' && values) {
      this.taskManagerLogin();
    }
  };

  render() {
    const { email, password, errorMessage } = this.state;
    const disabled = !email || !password;

    let error = null;
    if (errorMessage) {
      error = <div>{errorMessage}</div>;
    }

    return (
      <div className="base-container">
        <div className="login-header">Task Manager</div>

        <div className="content">
          <div className="image">
            <img src={Clipboard} alt="" />
          </div>
          <form className="form">
            <div className="form-group">
              <label htmlFor="email">Email</label>
              <input
                id="email"
                type="email"
                name="email"
                placeholder="email"
                onChange={this.handleInputChange}
                onKeyDown={this.handleKeyDown}
                required={true}
              />
            </div>
            <div className="form-group">
              <label htmlFor="password">Password</label>
              <input
                id="password"
                type="password"
                name="password"
                placeholder="password"
                onChange={this.handleInputChange}
                onKeyDown={this.handleKeyDown}
                required={true}
              />
            </div>
          </form>
          <div className="error-message">{error}</div>
        </div>
        <LoginFooter onLoginClick={this.handleLogin} disabled={disabled}/>
      </div>
    );
  }
}

export default LoginForm;

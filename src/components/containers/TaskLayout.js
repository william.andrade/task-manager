import React from "react";
import Header from "../shared/Header";
import "../../styles/TaskLayout.scss";

function TaskLayout(props) {
  return (
    <div className="task-layout">
      <Header history={props.history} />
      {props.children}
    </div>
  );
}

export default TaskLayout;

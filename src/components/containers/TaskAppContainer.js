import React, { Component } from "react";
import { getTasks, updateTask } from "../../services/taskService";
import TaskCard from "../tasks/TaskCard";
import CaretUp from "../../assets/caret-up.svg";
import CaretDown from "../../assets/caret-down.svg";
import TaskLayout from "./TaskLayout";
import NoTaskDisplay from "../tasks/NoTaskDisplay";
import { v4 as uuid } from "uuid";
import "../../styles/TaskAppContainer.scss";
import { NOT_COMPLETED } from "../../constants";

class TaskAppContainer extends Component {
  state = {
    tasks: [],
    tasksLoaded: false,
  };

  componentDidMount() {
    getTasks().then((data) => {
      const filteredData = data.filter(task => task.description.length !== 0);
      this.setState({ tasks: filteredData, tasksLoaded: true });
    });
  }

  handleDueDateSort = (event, order) => {
    event.preventDefault();

    const { tasks } = this.state;
    let sortedTasks = tasks.slice();

    if (order === "asc") {
      sortedTasks = sortedTasks.sort((a, b) => {
        return new Date(a.dueDate) - new Date(b.dueDate); //ascending order
      });
    }
    if (order === "desc") {
      sortedTasks = sortedTasks.sort((a, b) => {
        return new Date(b.dueDate) - new Date(a.dueDate); //descending order
      });
    }

    this.setState({ tasks: sortedTasks });
  };

  // handles creating a new task with the TaskDetails component
  // initialize all fields except for id to be empty
  handleCreateNewTask = (event) => {
    event.preventDefault();
    // assign a unique id
    const newTask = {
      id: uuid(),
      title: "",
      description: "",
      dueDate: "",
      status: NOT_COMPLETED,
    };

    this.props.history.push({pathname: `/task-manager/task/${newTask.id}`, state: {taskData: newTask, new: true}})
  };

  // handles task status updates from TaskCard component, not TaskDetail
  handleTaskStatusChange = (task) => {
    updateTask(task).then((updatedTasks) => {
      this.setState({ tasks: updatedTasks });
    });
  };

  render() {
    const { tasks, tasksLoaded } = this.state;
    let taskFeedContent = null;
    if (tasksLoaded && tasks.length > 0) {
      taskFeedContent = tasks.map((task) => (
        <TaskCard
          key={task.id}
          task={task}
          history={this.props.history}
          onTaskStatusChange={this.handleTaskStatusChange}
        />
      ));
    }
    if (tasksLoaded && tasks.length === 0) {
      taskFeedContent = <NoTaskDisplay />;
    }

    return (
      <TaskLayout history={this.props.history}>
        <div className="task-feed">{taskFeedContent}</div>

        <div className="task-buttons-container">
          <button
            type="submit"
            className="create-new-button"
            onClick={this.handleCreateNewTask}
          >
            Add New +
          </button>

          <div>
            <img
              id="ascending-arrow"
              title="Sort in ascending order"
              src={CaretUp}
              alt="caret-up"
              onClick={(event) => this.handleDueDateSort(event, "asc")}
            />
            <img
              id="descending-arrow"
              title="Sort in descending order"
              src={CaretDown}
              alt="caret-down"
              onClick={(event) => this.handleDueDateSort(event, "desc")}
            />
          </div>
        </div>
      </TaskLayout>
    );
  }
}

export default TaskAppContainer;

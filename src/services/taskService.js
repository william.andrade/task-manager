import {handleErrors} from "../utils/utils";

const baseServiceUrl = "http://localhost:3000";
const headers = { "Content-type": "application/json; charset=UTF-8" };


export async function getTasks() {
  // data for TaskAppContainer
  try {
    const response = await fetch(`${baseServiceUrl}/api/tasks/`);
    const tasks = response.json();
    return tasks;
  } catch (error) {
    handleErrors(error);
  }
}

export async function getTask(taskId) {
  // data for TaskDetail
  try {
    const response = await fetch(`${baseServiceUrl}/api/tasks/${taskId}`);
    const task = response.json();
    return task;
  } catch (error) {
    handleErrors(error);
  }
}

export async function updateTask(task) {
  // update a task from TaskDetail
  try {
    const putOptions = {
      method: "PUT",
      headers: headers,
      body: JSON.stringify(task),
    };
    const response = await fetch(
      `${baseServiceUrl}/api/tasks/update/${task.id}`,
      putOptions
    );
    const data = response.json();
    return data;
  } catch (error) {
    handleErrors(error);
  }
}

export async function deleteTask(task) {
  // Delete a task from TaskDetail
  try {
    const deleteOptions = {
      method: "DELETE",
      headers: headers,
    };
    const response = await fetch(
      `${baseServiceUrl}/api/tasks/delete/${task.id}`,
      deleteOptions
    );
    const data = response.json();
    return data;
  } catch (error) {
    handleErrors(error);
  }
}

export async function postNewTask(task) {
  // post a new task to the server
  try {
    const postOptions = {
      method: "POST",
      headers: headers,
      body: JSON.stringify(task),
    };
    const response = await fetch(
      `${baseServiceUrl}/api/tasks/create-new/`,
      postOptions
    );
    const data = response.json();
    return data;
  } catch (error) {
    handleErrors(error);
  }
}

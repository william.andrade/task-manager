import {handleErrors} from "../utils/utils";

const baseServiceUrl = "http://localhost:3000";

export async function login(email, password) {
  try {
    const response = await fetch(`${baseServiceUrl}/user/login`, {
      method: "POST",
      headers: { "Content-type": "application/json; charset=UTF-8"},
      body: JSON.stringify({ email: email, password: password }),
    });

    const data = await response.json();
    return data;
  } catch (error) {
    handleErrors(error);
  }
}

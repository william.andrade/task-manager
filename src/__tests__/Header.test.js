import React from "react";
import renderer from "react-test-renderer";
import Header from "../components/shared/Header";

describe("Header", () => {
  it("renders Header component", () => {
    const header = renderer.create(<Header />).toJSON();
    expect(header).toMatchSnapshot();
  });
});

import React from "react";
import renderer from "react-test-renderer";
import LoginForm from "../components/login/LoginForm";

describe("LoginForm", () => {
  it("should render LoginForm component", () => {
    const loginFormTree = renderer.create(<LoginForm />);
    expect(loginFormTree).toMatchSnapshot();
  });
});

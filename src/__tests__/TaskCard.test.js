import React from "react";
import { configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import TaskCard from "../components/tasks/TaskCard";
import {NOT_COMPLETED, COMPLETED_STATUS} from "../constants";
import { shallow } from "enzyme";
import renderer from "react-test-renderer";

configure({ adapter: new Adapter() });

const mockTask = {
  id: "c4b9515f-30d0-4ec3-a689-2db29862c57d",
  title: "Testing new task creation ",
  description: "Testing new task creation again",
  dueDate: "2020-11-28",
  status: NOT_COMPLETED,
};


describe("TaskCard", () => {

  it ("should render TaskCard component", () => {
    const taskCardTree = renderer.create(<TaskCard task={mockTask}/>).toJSON();
    expect(taskCardTree).toMatchSnapshot();

  })

  it("should change the status to completed correctly", () => {
    const mockOnTaskStatusChange = jest.fn();
    const wrapper = shallow(
      <TaskCard task={mockTask} onTaskStatusChange={mockOnTaskStatusChange} />
    );
    const mockEvent = { stopPropagation: jest.fn() };
    const mockNewTask = { ...mockTask, status: COMPLETED_STATUS};
    wrapper.instance().handleStatusChange(mockEvent);
    expect(mockOnTaskStatusChange).toHaveBeenCalledWith(mockNewTask);
  });

});

import { login } from "../services/authService";

const validAuthResponse = {
  token: "sdfshbGciOiJIUzI1NiIsInR5cCI6Ik",
  user: { _id: "5e755f783b2356001bff3123" },
};

beforeEach(function () {
  // global fetch that mocks all fetch calls in code
  global.fetch = jest.fn().mockImplementation(() => {
    return new Promise((resolve, reject) => {
      resolve({
        json: function () {
          return validAuthResponse;
        },
      });
    });
  });
});

describe("authService", () => {
  it("Should return object with correct credentials", async () => {
    const response = await login("test@test.com", "password");
    expect(response).toHaveProperty("user");
    expect(response).toHaveProperty("token");
  });
});

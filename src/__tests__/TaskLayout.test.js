import React from "react";
import renderer from "react-test-renderer";
import TaskLayout from "../components/containers/TaskLayout";

describe("TaskLayout", () => {
  it("renders TaskLayout component", () => {
    const taskLayoutTree = renderer.create(<TaskLayout />).toJSON();
    expect(taskLayoutTree).toMatchSnapshot();
  });
});

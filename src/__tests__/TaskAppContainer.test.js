import React from "react";
import { configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import renderer from "react-test-renderer";
import TaskAppContainer from "../components/containers/TaskAppContainer";
import { shallow } from "enzyme";
import { NEW_STATUS, COMPLETED_STATUS } from "../constants";

configure({ adapter: new Adapter() });

const mockTasks = [
  {
    id: "c4b9515f-30d0-4ec3-a689-2db29862c57d",
    title: "Take out Rocky",
    description: "walked the dog",
    dueDate: "2020-11-28",
    status: COMPLETED_STATUS,
  },
  {
    id: "c4b9515f-30d0-4ec3-a689-2db29862caaa",
    title: "Take out Kovu ",
    description: "Neighbor asked me to take Kovu out, should I charge?",
    dueDate: "2020-11-30",
    status: NEW_STATUS,
  },
];

describe("TaskAppContainer", () => {
  it("should render TaskAppContainer", () => {
    const taskAppContainer = renderer.create(<TaskAppContainer />).toJSON();
    expect(taskAppContainer).toMatchSnapshot();
  });

  it("should render two TaskCard components", () => {
    const wrapper = shallow(<TaskAppContainer />);
    wrapper.setState({ tasks: mockTasks, tasksLoaded: true });
    expect(wrapper.state("tasks")).toHaveLength(2);
    expect(wrapper.find("TaskCard")).toHaveLength(2);
  });

  it("should render NoTaskDisplay component", () => {
    const wrapper = shallow(<TaskAppContainer />);
    wrapper.setState({ tasks: [], tasksLoaded: true });
    expect(wrapper.state("tasks")).toHaveLength(0);
    expect(wrapper.find("NoTaskDisplay")).toHaveLength(1);
  });
});

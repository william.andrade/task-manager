import { formatDate } from "../utils/utils";
import { COMPLETED_STATUS } from "../constants";

const mockTask = {
  id: "c4b9515f-30d0-4ec3-a689-2db29862c57d",
  title: "Take out Rocky",
  description: "walked the dog",
  dueDate: "2020-11-28",
  status: COMPLETED_STATUS,
};

describe("Util functions", () => {
  it("Should format date to MM/DD/YYYY format", () => {
    const formattedDate = formatDate(mockTask.dueDate);
    expect(formattedDate).toEqual("11/28/2020");
  });
});

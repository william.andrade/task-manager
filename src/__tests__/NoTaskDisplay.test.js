import React from "react";
import renderer from "react-test-renderer";
import NoTaskDisplay from "../components/tasks/NoTaskDisplay";

describe("TaskLayout", () => {
  it("renders TaskLayout component", () => {
    const noTaskDisplay = renderer.create(<NoTaskDisplay />).toJSON();
    expect(noTaskDisplay).toMatchSnapshot();
  });
});

import React from "react";
import { configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import renderer from "react-test-renderer";
import TaskDetails from "../components/tasks/TaskDetails";
import {NOT_COMPLETED} from "../constants";

const mockTasks =
  {
    id: "c4b9515f-30d0-4ec3-a689-2db29862caaa",
    title: "Take out Kovu ",
    description: "Neighbor asked me to take Kovu out, should I charge?",
    dueDate: "2020-11-30",
    status: NOT_COMPLETED,
  };

configure({ adapter: new Adapter() });

describe("TaskDetails", () => {
  it("should render TaskDetails component", () => {
    const mockLocation = {
      state: { taskId: "c4b9515f-30d0-4ec3-a689-2db29862c57d" },
    };
    const mockParams = {
      params: {id: mockTasks.id}
    }
    const taskDetails = renderer
      .create(<TaskDetails location={mockLocation} match={mockParams}/>)
      .toJSON();
    expect(taskDetails).toMatchSnapshot();
  });
});

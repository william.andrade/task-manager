import React from "react";
import renderer from "react-test-renderer";
import LoginFooter from "../components/login/LoginFooter";

describe("LoginFooter", () => {
  it("should render LoginFooter", () => {
    const loginFooter = renderer.create(<LoginFooter />).toJSON();
    expect(loginFooter).toMatchSnapshot();
  });

  it("should render LoginFooter with disabled button", () => {
    const loginFooter = renderer.create(<LoginFooter disabled={true} />);
    const instance = loginFooter.root;
    const loginButton = instance.findByType("button");
    expect(loginButton.props.style).toMatchObject({ backgroundColor: "grey" });
  });

  it("should render LoginFooter with enabled button", () => {
    const loginFooter = renderer.create(<LoginFooter disabled={false} />);
    const instance = loginFooter.root;
    const loginButton = instance.findByType("button");
    expect(loginButton.props.style).toMatchObject({
      backgroundColor: "#25bb87",
    });
  });
});

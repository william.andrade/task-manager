export const COMPLETED_STATUS = "Completed";
export const NOT_COMPLETED = "Not completed";
export const NO_TASKS_MESSAGE = "You have no active tasks to display!";

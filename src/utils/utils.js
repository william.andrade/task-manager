//https://stackoverflow.com/questions/32378590/set-date-input-fields-max-date-to-today
export function setMinDate() {
  // sets minimumDate to today on the datepicker in TaskDetail
  let today = new Date();
  let dd = today.getDate();
  let mm = today.getMonth() + 1;
  let yyyy = today.getFullYear();
  if (dd < 10) {
    dd = "0" + dd;
  }
  if (mm < 10) {
    mm = "0" + mm;
  }
  today = yyyy + "-" + mm + "-" + dd; // date inputs need to be in yyyy-mm-format
  return today;
}

//https://stackoverflow.com/questions/28246788/convert-yyyy-mm-dd-to-mm-dd-yyyy-in-javascript
export function formatDate(inputDate) {
  let date = new Date(inputDate);

  if (!isNaN(date.getTime())) {
    // Months use 0 index. aka January starts at 0 so add 1 to make it 1st month
    // Days use 0 index.
    return (
      date.getMonth() + 1 + "/" + (date.getDate() + 1) + "/" + date.getFullYear()
    );
  }
}

export function handleErrors(error) {
  throw new Error(error.message);
}
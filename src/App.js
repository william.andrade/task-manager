import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import LoginForm from "./components/login/LoginForm";
import TaskDetails from "./components/tasks/TaskDetails";
import TaskAppContainer from "./components/containers/TaskAppContainer";

function App() {
  return (
    <div>
      <Switch>
        <Route
          exact
          path={"/"}
          render={() => {
            return <Redirect to={"/login"} />;
          }}
        />

        <Route
          exact
          path={"/login"}
          render={(props) => <LoginForm {...props} />}
        />

        <Route
          exact
          path={"/task-manager"}
          render={(props) => <TaskAppContainer {...props} />}
        />

        <Route
          exact
          path={`/task-manager/task/:id`}
          render={(props) => <TaskDetails {...props} />}
        />
      </Switch>
    </div>
  );
}

export default App;
